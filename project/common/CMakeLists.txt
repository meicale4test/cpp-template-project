# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Copyright 2020 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

project(common
  LANGUAGES
    CXX
  DESCRIPTION
    "A collection of common code."
)

#------------------------------------------------------------------------------#
# Project files                                                                #
#------------------------------------------------------------------------------#

set(API_HEADERS
  include/common/floating_point.hpp
  include/common/point.hpp
)

set(HEADERS
  ${API_HEADERS}
)

set(SOURCE
  # Add source files...
)

#------------------------------------------------------------------------------#
# Project                                                                      #
#------------------------------------------------------------------------------#

add_library("${PROJECT_NAME}" INTERFACE)
add_library(Ctp::Common ALIAS ${PROJECT_NAME})

source_group("api" FILES ${API_HEADERS})

#target_init("${PROJECT_NAME}")

#------------------------------------------------------------------------------#
# Project directories                                                          #
#------------------------------------------------------------------------------#

target_include_directories("${PROJECT_NAME}"
  INTERFACE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
)
