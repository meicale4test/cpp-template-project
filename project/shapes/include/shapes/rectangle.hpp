/*
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 * 
 * Copyright 2020 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_SHAPES_RECTANGLE_HPP
#define CTP_SHAPES_RECTANGLE_HPP

#include "common/point.hpp"

#include <boost/noncopyable.hpp>

namespace Ctp {
namespace Shapes {

class Rectangle
    :   private boost::noncopyable
{

public:

    virtual ~Rectangle () = default;


    [[nodiscard]]
    virtual Common::Point getTopLeft () const = 0; 

    [[nodiscard]]
    virtual Common::Point getTopRight () const = 0; 

    [[nodiscard]]
    virtual Common::Point getBottomLeft () const = 0; 

    [[nodiscard]]
    virtual Common::Point getBottomRight () const = 0; 


    [[nodiscard]]
    virtual double getWidth () const = 0; 

    [[nodiscard]]
    virtual double getHeight () const = 0; 

    [[nodiscard]]
    virtual double getPerimeter () const = 0; 

    [[nodiscard]]
    virtual double getArea () const = 0; 


    [[nodiscard]]
    virtual bool contains ( Common::Point ) const = 0; 

    [[nodiscard]]
    virtual bool intersects ( Rectangle const & _other ) const = 0; 

    [[nodiscard]]
    virtual bool covers ( Rectangle const & _other ) const = 0; 

};

} // namespace Shapes
} // namespace Ctp

#endif // CTP_SHAPES_RECTANGLE_HPP
