/*
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 * 
 * Copyright 2020 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/shapes_factory_impl.hpp"

#include "src/rectangle_impl.hpp"

namespace Ctp {
namespace Shapes {

std::unique_ptr< Rectangle >
ShapesFactoryImpl::createRectangle (
    Common::Point _topLeftPoint,
    Common::Point _rightBottomPoint
) const
{
    return std::make_unique< RectangleImpl >( _topLeftPoint, _rightBottomPoint );
}

std::unique_ptr< Rectangle >
ShapesFactoryImpl::createRectangle (
    Common::Point _point,
    double _width,
    double _height
) const
{
    return std::make_unique< RectangleImpl >( _point, _width, _height );
}

} // namespace Shapes
} // namespace Ctp
