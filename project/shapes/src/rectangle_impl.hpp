/*
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 * 
 * Copyright 2020 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_SHAPES_SRC_RECTANGLEIMPL_HPP
#define CTP_SHAPES_SRC_RECTANGLEIMPL_HPP

#include "shapes/rectangle.hpp"

namespace Ctp {
namespace Shapes {

class RectangleImpl final
    :   public Rectangle
{

public:

    RectangleImpl ( Common::Point _topLeftPoint, Common::Point _rightBottomPoint );

    RectangleImpl ( Common::Point _point, double _width, double _height );


    Common::Point getTopLeft () const override;

    Common::Point getTopRight () const override;

    Common::Point getBottomLeft () const override;

    Common::Point getBottomRight () const override;


    double getWidth () const override;

    double getHeight () const override;

    double getPerimeter () const override;

    double getArea () const override;


    bool contains ( Common::Point ) const override;

    bool intersects ( Rectangle const & _other ) const override;

    bool covers ( Rectangle const & _other ) const override;

private:
	double m_xTL, m_yTL,
			m_xBR, m_yBR;
};

} // namespace Shapes
} // namespace Ctp

#endif // CTP_SHAPES_SRC_RECTANGLEIMPL_HPP
