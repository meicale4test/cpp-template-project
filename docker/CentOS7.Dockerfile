# Never try to install CentOS 7 SCL packages and build with them something in a
# same Docker file. This is just a one big pain. But if you still want...
#
# First of all I tried a below solution, that is based on:
#   - https://stackoverflow.com/questions/20635472
#   - https://stackoverflow.com/questions/55901985
#
# CentOS7.Dockerfile
#   ...
#   RUN echo "source scl_source enable devtoolset-8" >> /etc/bashrc && \
#       echo "source scl_source enable llvm-toolset-7.0" >> /etc/bashrc
#
#   Make SCL avaible globaly in user environment.
#   SHELL ["/bin/bash", "-c", "source /etc/bashrc"]
#   ...
#   # Make SCL avaible to anyone how use this docker image.
#   ENTRYPOINT ["/bin/bash", "-c", "source /etc/bashrc"]
#
# I don't know why but the above Docker file doesn't work at all. It literaly
# breaks GitLab CI. CI doesn't run 'script:' section with a such docker and
# already report successful result.
#
# Working solution is based on https://github.com/sclorg/devtoolset-container/
# docker container for 7-toolchain.

FROM centos:7

COPY scripts/os/setup_centos7.sh /tmp/
RUN bash /tmp/setup_centos7.sh

RUN echo "unset BASH_ENV PROMPT_COMMAND ENV" >> /etc/scl_enable && \
    echo "source scl_source enable devtoolset-8" >> /etc/scl_enable && \
    echo "source scl_source enable llvm-toolset-7.0" >> /etc/scl_enable

# Enable the SCL for all bash scripts.
ENV BASH_ENV=/etc/scl_enable \
    ENV=/etc/scl_enable \
    PROMPT_COMMAND=". /etc/scl_enable"

COPY cmake/Conan.cmake CMakeLists.txt
# We use `bash -c` here to catch our new `BASH_ENV`.
# https://stackoverflow.com/a/29023834
RUN bash -c 'CC=clang CXX=clang++ cmake -S. -Bbuild -DFETCH_CONAN_DEPENDENCIES=1 -DCMAKE_BUILD_TYPE=Release \
 && conan remove "*" -s -b -f'
RUN bash -c 'CC=gcc CXX=g++ cmake -S. -Bbuild -DFETCH_CONAN_DEPENDENCIES=1 -DCMAKE_BUILD_TYPE=Release \
 && conan remove "*" -s -b -f'
