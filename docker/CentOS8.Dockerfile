FROM centos:8

COPY scripts/os/setup_centos8.sh /tmp/
RUN bash /tmp/setup_centos8.sh

COPY cmake/Conan.cmake CMakeLists.txt
RUN CC=clang CXX=clang++ cmake -S. -Bbuild -DFETCH_CONAN_DEPENDENCIES=1 -DCMAKE_BUILD_TYPE=Release \
 && conan remove "*" -s -b -f
RUN CC=gcc CXX=g++ cmake -S. -Bbuild -DFETCH_CONAN_DEPENDENCIES=1 -DCMAKE_BUILD_TYPE=Release \
 && conan remove "*" -s -b -f
