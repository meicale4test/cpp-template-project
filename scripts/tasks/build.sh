#!/usr/bin/env bash

declare BUILD_TYPE="${1}"
declare BUILD_FOLDER="./build/${BUILD_TYPE}"

if [[ ! -d "${BUILD_FOLDER}" ]]; then
  echo -e "\033[0;31m'${BUILD_FOLDER}' is missing! Run configuration step first!\033[0m"
  exit 1
fi

declare -i NUMBER_OF_PROCESSORS=$(( $(nproc)/2 ))

echo -e "\033[1;33mcmake --build "${BUILD_FOLDER}" -j${NUMBER_OF_PROCESSORS}\033[0m"
cmake --build "${BUILD_FOLDER}" -j${NUMBER_OF_PROCESSORS}
