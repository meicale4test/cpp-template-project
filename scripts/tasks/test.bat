set BUILD_TYPE=%1
set BUILD_FOLDER=.\\build\\%1

if not exist %BUILD_FOLDER% (
  echo '%BUILD_FOLDER%' is missing. Run configuration step first.
)

setx CTEST_OUTPUT_ON_FAILURE 1

echo Run: ctest -j%NUMBER_OF_PROCESSORS% --timeout 3
ctest -j%NUMBER_OF_PROCESSORS% --timeout 3
