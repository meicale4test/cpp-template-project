set BUILD_TYPE=%1
set BUILD_FOLDER=.\\build\\%1

if not exist %BUILD_FOLDER% mkdir %BUILD_FOLDER%

echo Run: cmake -S. -B %BUILD_FOLDER% -DCMAKE_BUILD_TYPE=%BUILD_TYPE%
cmake -S. -B %BUILD_FOLDER% -DCMAKE_BUILD_TYPE=%BUILD_TYPE%
