#!/usr/bin/env bash

declare BUILD_TYPE="${1}"
declare BUILD_FOLDER="./build/${BUILD_TYPE}"

if [[ ! -d "${BUILD_FOLDER}" ]]; then
  mkdir -p "${BUILD_FOLDER}"
fi

echo -e "\033[1;33mcmake -S. -B "${BUILD_FOLDER}" -DCMAKE_BUILD_TYPE=${BUILD_TYPE}\033[0m"
cmake -S. -B "${BUILD_FOLDER}" -DCMAKE_BUILD_TYPE=${BUILD_TYPE}
