#!/usr/bin/env bash

declare BUILD_TYPE="${1}"
declare BUILD_FOLDER="./build/${BUILD_TYPE}"

if [[ ! -d "${BUILD_FOLDER}" ]]; then
  echo -e "\033[0;31m'${BUILD_FOLDER}' is missing! Run configuration step first!\033[0m"
  exit 1
fi

cd "${BUILD_FOLDER}"

declare -i NUMBER_OF_PROCESSORS=$(( $(nproc)/2 ))
export CTEST_OUTPUT_ON_FAILURE=1

echo -e "\033[1;33mctest -j${NUMBER_OF_PROCESSORS} --timeout 3\033[0m"
ctest -j${NUMBER_OF_PROCESSORS} --timeout 3
