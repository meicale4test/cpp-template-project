set BUILD_TYPE=%1
set BUILD_FOLDER=.\\build\\%BUILD_TYPE%

if not exist %BUILD_FOLDER% (
  echo '%BUILD_FOLDER%' is missing. Run configuration step first.
)

echo Run: cmake --build %BUILD_FOLDER% -j%NUMBER_OF_PROCESSORS%
cmake --build %BUILD_FOLDER% -j%NUMBER_OF_PROCESSORS%
