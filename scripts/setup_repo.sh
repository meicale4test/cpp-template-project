#!/usr/bin/env bash

set -e

git config core.hooksPath "$(git rev-parse --show-toplevel)/hooks"
