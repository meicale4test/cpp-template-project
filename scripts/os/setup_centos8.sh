#!/usr/bin/env bash

set -e

#------------------------------------------------------------------------------#
# Official Red Hat Enterprise Linux RPM software packages                      #
#------------------------------------------------------------------------------#

INSTALL_PKGS=(
    "clang"             # 9.0.1
    "gcc"               # 8.2.1
    "gcc-c++"           # 8.2.1
    "gcc-gfortran"      # 8.2.1
    "gdb"               # 8.2
    "make"              # 4.2.1
    "python3-pip"       # python 3.6 + pip3 9.0.3
)

## Install packages

yum install -y --setopt=tsflags=nodocs ${INSTALL_PKGS[@]}
rpm -V ${INSTALL_PKGS[@]}
yum -y clean all --enablerepo='*'

#------------------------------------------------------------------------------#
# Python                                                                       #
#------------------------------------------------------------------------------#

# CentOS 8 comes with CMake 3.11.4 and unlike CentOS 7, there is no yum way to get
# a new version.
# We can use Snap, but it depends on systemd. Guess what? Docker is not a friend
# of systemd.
# But thanks to https://cliutils.gitlab.io/modern-cmake/chapters/intro/installing.html
# we can install cmake 3.18.0 (24/07/2020) using pip.
pip3 install cmake conan
