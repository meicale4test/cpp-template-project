#!/usr/bin/env bash

set -e

#conan remote add conan-center https://conan.bintray.com True
conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan True
