include(cmake/build/CompilerWarnings.cmake)
include(cmake/lint/Sanitizers.cmake)
include(cmake/lint/TimeTrace.cmake)

function(target_init project_name)

  set_target_properties(${project_name}
    PROPERTIES
      CXX_STANDARD ${CMAKE_CXX_STANDARD}
      CXX_STANDARD_REQUIRED ON
      CXX_EXTENSIONS OFF
  )

  # Standard compiler warnings
  set_project_warnings(${project_name})

  # Sanitizer options if supported by compiler
  enable_sanitizers(${project_name})

  # Time trace options if supported by compiler
  enable_time_trace(${project_name})

endfunction()

include(GenerateExportHeader)

function(target_dll_init project_name)

  target_init(${project_name})

  # Use "hidden" symbol visibility by default
  set_target_properties(${project_name}
    PROPERTIES
      CXX_VISIBILITY_PRESET hidden
      VISIBILITY_INLINES_HIDDEN 1
  )

  generate_export_header(${project_name}
    PREFIX_NAME
    	CTP_
  )

endfunction()
