macro(run_conan)
  # Download automatically, you can also just copy the conan.cmake file
  if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
    message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
    file(DOWNLOAD "https://github.com/conan-io/cmake-conan/raw/v0.15/conan.cmake" "${CMAKE_BINARY_DIR}/conan.cmake")
  endif()

  include(${CMAKE_BINARY_DIR}/conan.cmake)

  conan_add_remote(
    NAME
      bincrafters
    INDEX
      1
    URL
      https://api.bintray.com/conan/bincrafters/public-conan
    VERIFY_SSL
      True
    )

  conan_cmake_run(
    BASIC_SETUP
    BUILD
      missing
    CMAKE_TARGETS
      # individual targets to link to
    REQUIRES
      boost/1.71.0@conan/stable
      docopt.cpp/0.6.3
    GENERATORS
      cmake_find_package
    OPTIONS
      boost:without_atomic=True
      boost:without_chrono=True
      boost:without_container=True
      boost:without_context=True
      boost:without_contract=True
      boost:without_coroutine=True
      boost:without_date_time=False
      boost:without_exception=False
      boost:without_fiber=True
      boost:without_filesystem=True
      boost:without_graph_parallel=True
      boost:without_graph=True
      boost:without_iostreams=True
      boost:without_locale=True
      boost:without_log=True
      boost:without_math=False
      boost:without_mpi=True
      boost:without_program_options=True
      boost:without_python=True
      boost:without_random=True
      boost:without_regex=True
      boost:without_serialization=True
      boost:without_stacktrace=False
      boost:without_system=True
      boost:without_test=False
      boost:without_thread=True
      boost:without_timer=True
      boost:without_type_erasure=True
      boost:without_wave=True
    SETTINGS
      compiler.cppstd=${CMAKE_CXX_STANDARD}
)
endmacro()


option(FETCH_CONAN_DEPENDENCIES "Fetch all conan packages to create a pre-build image in CI mode." OFF)
mark_as_advanced(FETCH_CONAN_DEPENDENCIES)

if(FETCH_CONAN_DEPENDENCIES)
  cmake_minimum_required(VERSION 3.15 FATAL_ERROR)
  project(ConanPackagesDownloader)
  run_conan()
endif()
